###
#
# Simple script to compare folders
# 
#
###


import tkinter as tk
from tkinter import filedialog
import os

root = tk.Tk()
root.withdraw()

dir_left = filedialog.askdirectory()
dir_right = filedialog.askdirectory()


lfiles = os.listdir(dir_left)
rfiles = os.listdir(dir_right)

# for file in lfiles:
#     if file in rfiles:
#         print (f"file exists {file}")
#     else:
#         print (f"file not exists {file}")

def list_duplicates(lfiles, rfiles):
    values = set(lfiles).intersection(set(rfiles))
    print("Files that exists on both folders")
    print(values)

def list_ones_on_left(lfiles, rfiles):
    values = set(lfiles).difference(set(rfiles))
    print(f"Files that not exists on {rfiles}")
    for f in values:
        print(f)


def list_ones_on_right(lfiles, rfiles):
    values = set(rfiles).difference(set(lfiles))
    print(f"Files that not exists on {lfiles}")
    for f in values:
        print(f)


#list_ones_on_left(lfiles, rfiles)
list_ones_on_right(lfiles, rfiles)